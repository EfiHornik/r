library(corrplot)
library(pROC)
library(ggplot2)
library(caTools)
library(lubridate)
library(dplyr)
library(rpart)
library(rpart.plot)
library(randomForest)
library(ROSE)
library(tm)
library(FSelector)
library(plyr)

setwd("C:/Users/USER1/exam datafile")

onion <- read.csv("OnionOrNot.csv")
onion.raw<-onion
summary(onion)


#Qus 1
change_name <- function(x){
  if (x=="1") return ('yes')
  return ("no")
}

onion$label <- sapply(onion$label,change_name)

onion$label<-as.factor(onion$label)

summary(onion)
str(onion)
onion$text<-as.character(onion$text)

table(onion$label)
lbls <- c("NO", "Yes")
pie(slices, labels = lbls, main="Labels Pie Chart")

#Qus 2

onion.corpus <- Corpus(VectorSource(onion$text))
onion.corpus[[1]][[1]]

clean.corpus <- tm_map(onion.corpus, removePunctuation)
clean.corpus[[1]][[1]]

clean.corpus <- tm_map(clean.corpus, removeNumbers)

clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))

clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())

clean.corpus <- tm_map(clean.corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean.corpus)
dim(dtm)
inspect(dtm[1:10,1:20])

vec<- seq(100,1000,length=180)
dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,vec))) 
conv_01 <- function(x){
  x <- ifelse(x>0,1,0)
  return (as.numeric(x))
}

dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)
dtm.df <- as.data.frame(dtm.final)

dtm.df$onion<- onion$label

filter <- sample.split(dtm.df$onion,SplitRatio = 0.7 )

dtm.df.train <- subset(dtm.df,filter==T)
dtm.df.test <- subset(dtm.df,filter==F)

dim(dtm.df.train)
dim(dtm.df.test)
dim(dtm.df)

#Qus 3

dtm.model <- glm(onion ~., family = binomial(link = 'logit'), data = dtm.df.train)
prediction <- predict(dtm.model, dtm.df.test, type = 'response')
hist(prediction)

actual <- dtm.df.test$onion
confusion_matrix <- table(actual,predition > 0.3)


precision <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[1,2])
recall <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[2,1]) 

#Decision tree
model.dt <- rpart(onion ~ ., dtm.df.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)


prediction.dt <- predict(model.dt,dtm.df.test)
actual <-dtm.df.test$onion
prediction.dt<-prediction[,"no"]
cf <- table(actual,prediction.dt > 0.3)

precision <- cf[2,2]/(cf[2,2] + cf[1,2])
recall <- cf[2,2]/(cf[2,2] + cf[2,1]) 

rocCurveLR <- roc(actual,prediction, direction = ">", levels = c("yes", "no"))
rocCurveDT <- roc(actual,prediction.dt, direction = ">", levels = c("yes", "no"))

plot(rocCurveLR, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveDT, col = 'blue',main = "ROC Chart")



#Qus 4

thwords <- dtm.df[,c("trump", "just", "man")]
filter <- sample.split(thwords$onion,SplitRatio = 0.7 )

thwords.train <- subset(dtm.df,filter==T)
thwords.test <- subset(dtm.df,filter==F)

model.thwords <- rpart(onion ~ ., thwords.train)
rpart.plot(model.thwords, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

